﻿#include <iostream>
#include <ctime>


int main() {

    struct tm buf {};
    time_t t = time(nullptr);
    localtime_s(&buf, &t);

    const int size = 5;    
    int sum = 0;

    int index = buf.tm_mday % size;

    int arr[size][size];

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            arr[i][j] = i + j;
        }
    }

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }

    for (int i = 0; i < size; ++i) {
        sum += arr[index][i];
    }

    std::cout << "==========" << std::endl;
    std::cout << "Index: " << index << std::endl;
    std::cout << "Sum: " << sum << std::endl;

    return 0;
}